+++
title = "OpenPGP CA at Free Software Foundation Europe"
keywords = ["fsfe"]
author = "Heiko"
date = 2021-02-09T20:30:00+01:00
banner = "empty.jpg"
+++

<!--
SPDX-FileCopyrightText: 2019-2021 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# An OpenPGP CA instance at FSFE

Over the past months, we have worked with
[Free Software Foundation Europe (FSFE)](https://fsfe.org/) to set up an 
instance of OpenPGP CA as a part of FSFE's infrastructure - and to 
integrate its functionality with FSFE's community database.

<!--more-->

The starting point for the project was that FSFE was evaluating how to 
roll out a 
[Web Key Directory (WKD)](https://tools.ietf.org/html/draft-koch-openpgp-webkey-service-11)
for their community members' OpenPGP keys.

From there, we proceeded to explore many more aspects of this
undertaking - in particular how to apply OpenPGP CA's main concept at 
FSFE:

One central purpose of OpenPGP CA is to appropriately certify the OpenPGP 
public keys it manages, so that anyone who considers that particular 
OpenPGP CA instance a reasonable trust anchor can set it as a 
[trusted introducer](/using/trusted_introducer/) on their system, and 
thus benefit from the certifications that this CA instance publishes.

For example, if you configure the FSFE CA as a trusted introducer on your 
system, you will automatically have an authenticated path to the keys of 
all FSFE community members who participate in the system. Those keys will 
then automatically show up as "trusted" in your OpenPGP software.

As long as you consider the procedures of the FSFE CA sufficient, 
you can rely on its certifications - and never have to worry about 
checking the fingerprint of FSFE community member's OpenPGP keys ever 
again.

Note that the FSFE CA performs a function that is not entirely dissimilar 
to the [Let's Encrypt](https://letsencrypt.org/) service - both are fully 
automated systems that certify cryptographic identities. 

# Collaboration

Working with [Max](https://fsfe.org/about/people/mehl/mehl.en.html) and
[Reinhard](https://wiki.fsfe.org/Supporters/reinhard) from the FSFE was a
pleasure. They had a lot of great feedback which helped improve OpenPGP CA.

The design and development of OpenPGP CA have always been driven by
collaboration with organizations who are planning to adopt our tools.
If your organization is interested in introducing OpenPGP CA, we'd be
happy to help! This could be as simple as discussing how to deploy
OpenPGP CA in your organization - or we could help integrate OpenPGP CA
into your infrastructure.

# Some technical details

Early on, we decided that OpenPGP CA's functionality should be accessed 
by FSFE's
[community database frontend](https://git.fsfe.org/fsfe-ams/fsfe-cd-front)
via a
[REST interface](https://gitlab.com/openpgp-ca/openpgp-ca/-/blob/master/src/restd/README.md).
Over the course of this project, we co-designed a new REST API for this 
purpose.
Our overall goal was to build a system that exposes the complexity - and 
power - of OpenPGP in a user friendly way.

Also, we wanted to improve on common pain points around OpenPGP usage.
For example: FSFE will notify users about upcoming expiration of 
their OpenPGP keys. While it is good practice to configure keys with a 
limited expiration, traditionally there are no mechanisms to remind users 
that their key is expiring soon.

Internally, OpenPGP CA relies heavily on
[Sequoia PGP](https://sequoia-pgp.org/). 
This well-designed and meticulously documented library makes writing 
higher order tools (such as OpenPGP CA) easier and safer than it has ever 
been.

# Towards making strong, federated authentication the new normal

The OpenPGP ecosystem has always offered powerful, decentralized 
mechanisms for users to reason about the keys of their communication partners.

Even more, OpenPGP allows users to delegate this task to their OpenPGP 
implementation, which can automatically reason about complex certification 
structures on the user's behalf. In PGP terminology, the graph of 
certifications (or "signatures") between user keys is called the 
"web-of-trust".

The expressiveness and decentral nature of this mechanism set OpenPGP
apart from pretty much any other solution in the field of cryptographic
communication.

We believe that an important step towards the future of federated, 
decentralized cryptographic communication is to develop better tooling to 

- help seed the "web-of-trust" with meaningful certifications, and
- improve the client side software that interprets these artifacts on  
  behalf of users (particularly email software that display the 
  authentication status of communication partners in its UI).

The first of these two points is the central mission of the OpenPGP CA 
project. We're proud and happy to have FSFE as both a user of our
software - and as a partner who shares this vision.
