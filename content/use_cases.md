+++
title = "Use cases"
keywords = ["use cases"]
+++

OpenPGP CA can be applied in a wide variety of scenarios, to serve a wide 
range of goals. This page outlines two of the problems that OpenPGP CA 
helps to address.

# Make your Organization's keys easy to discover

If you rely on OpenPGP, you probably agree that it can be unreasonably 
hard to find the OpenPGP key for a communication partner.
To improve this situation, it makes sense for organizations to publish the 
OpenPGP keys of their members.

With OpenPGP CA, your organization can manage a repository of OpenPGP 
public keys - and easily publish those keys as a Web Key Directory (WKD).

In most cases, WKD is the best approach for organizations that 
want to run their own key distribution service. WKD is a standardized,
federated scheme that is well-supported by existing tools (such as GnuPG and
Thunderbird). It is usually preferable to running a local keyserver. 
Client software will not automatically find new keyservers, but it will 
automatically find new WKD instances.

WKD is easy to deploy on your existing infrastructure: all you need is 
control over your domain's DNS configuration and a webserver.

*TL;DR: OpenPGP CA offers tooling to centrally manage your organization's 
OpenPGP public keys, as well as to publish them via WKD. This solves the 
problem of (automated) key discovery.*


# Authentication made easy

To reliably protect your communication, it is vital that you authenticate 
the cryptographic keys of your communication partners.
Authentication guards against both
[active attacks](https://en.wikipedia.org/wiki/Man-in-the-middle_attack)
and user errors.

In practical terms, authentication means: following procedures to make sure 
you are using the correct key for your intended communication partner.

In the OpenPGP space, the burden of authentication has traditionally been 
put on each individual user: users were asked to personally verify the OpenPGP
fingerprints for all their communication partners.
This approach is arguably impractical in almost all circumstances.

However, the OpenPGP standard offers a powerful set of mechanisms which 
can just as easily be used to implement other authentication regimes.
In fact, the flexible (and inherently decentralized) approach of OpenPGP 
to authentication is one of its distinguishing features.

So, to achieve strong authentication without putting an unreasonable 
burden on users, OpenPGP CA facilitates an approach where users can 
delegate the work of authentication to trusted third parties. This 
way, authentication can become the norm, while reducing user effort.

The notion that each user should personally verify the OpenPGP keys for 
all of their communication partners is based on the idea that users 
should not rely on third parties for the security of their digital 
communication. However, most users' IT security already hinges crucially 
on the work of in-house IT specialists anyway.
In such contexts, requiring individual users to personally perform 
authentication serves no meaningful security purpose, while 
disproportionately complicating the use of OpenPGP.

OpenPGP CA offers a practical solution to make OpenPGP usage easier as 
well as safer, by making authenticated communication the default 
expectation - without relying on external third parties (whose goals 
might not be aligned with yours).

*TL;DR: Key management and checking fingerprints is a pain, which hinders 
the adoption and secure use of OpenPGP. OpenPGP CA largely moves these 
responsibilities to someone who is trusted by users.*

# Learn more

One of our central objectives is to make OpenPGP CA easy to integrate 
with your existing infrastructure. We currently offer CLI-based
and REST-based interfaces for our functionality.

Learn more [about the ideas and concepts of OpenPGP CA](/background/) and how 
to [use it in your organization](/tutorials/).

Contact us to talk about how to best integrate OpenPGP CA with your 
infrastructure and workflows!
