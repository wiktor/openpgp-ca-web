+++
title = "Background"
keywords = ["background"]
+++

Learn about the ideas behind OpenPGP CA: what does it offer, how does it work?
