+++
title = "Features"
keywords = ["features"]
+++

# Manage revocation certificates

The design of OpenPGP CA leans strongly towards user autonomy in the
handling of OpenPGP keys. In particular, we do not support key
escrow (i.e. centrally managing private key material).

However, as an optional feature, we support centrally managing revocation
certificates in an organization's OpenPGP CA instance.

Possible use cases for this feature include:

- your organization can revoke the binding of the OpenPGP key to the User
  ID that links the key to your organization.
- when a user loses access to their OpenPGP key, your organization can
  centrally revoke the key, thus notifying external parties that they
  should not encrypt to that key anymore.
