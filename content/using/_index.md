+++
title = "Using an existing OpenPGP CA instance"
keywords = ["tutorials"]
+++

If your organization is running an OpenPGP CA instance - or you are 
communicating with members of an organization that is running an OpenPGP 
CA instance, you will probably want to make use of the authentication 
services that CA is offering to you.
