+++
title = "Using an OpenPGP CA instance"
keywords = ["tutorial", "trusted introducer", "fsfe"]
weight = 10
+++

<!--
SPDX-FileCopyrightText: 2019-2021 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

To take advantage of the certifications that an OpenPGP CA instance makes, 
you need to configure your OpenPGP software (such as GnuPG) to recognize 
the CA key of that organization.

In technical terms, to do this, you configure the key of the CA as a 
*trusted introducer* on your system.

The benefit of this is that your OpenPGP software (including your email 
software) can then automatically recognize all OpenPGP keys that have been
certified by that CA as "trusted".

In this tutorial, we will configure the OpenPGP CA instance of the
[Free Software Foundation Europe](https://fsfe.org) as a trusted 
introducer in our GnuPG setup.

Afterwards, GnuPG will consider the OpenPGP keys of FSFE community members 
authenticated (for all FSFE community members who have uploaded their 
keys to the FSFE CA).

Traditionally, if you wanted to communicate with `alice@fsfe.org` and 
`bob@fsfe.org`, you personally needed to follow some procedure to check that 
you had the correct OpenPGP keys for both of these users (e.g. meeting 
both Alice and Bob in person and comparing fingerprints).

When setting the FSFE CA key as a trusted introducer, it is sufficient 
that you check the FSFE CA key, and tell your PGP software that you trust 
this CA to certify keys with email addresses at `fsfe.org`. Afterwards, 
your software can figure out by itself when keys have been certified by 
the FSFE CA, and consider these keys trustworthy.

To do this, first we retrieve a copy of the FSFE CA key from FSFE's WKD 
server:

```
$ gpg --locate-external-keys --auto-key-locate wkd openpgp-ca@fsfe.org

gpg: key 0x699D9E7F63B40E79: public key "FSFE OpenPGP CA <openpgp-ca@fsfe.org>" imported
gpg: Total number processed: 1
gpg:               imported: 1
```

Next, we set this CA key as a trusted introducer for OpenPGP keys 
with User IDs in the domain `fsfe.org`. Note that the following command uses 
the FSFE CA key's fingerprint. You probably want to verify that the 
fingerprint published in this tutorial matches the one published on
[https://keys.fsfe.org/](https://keys.fsfe.org/) before instructing your 
GnuPG instance to consider that key as a CA for `fsfe.org` with the 
following command.

```
$ gpg --edit-key 'A30A C9F8 FA7A 883C 0060  3070 699D 9E7F 63B4 0E79'
gpg (GnuPG) 2.2.20; Copyright (C) 2020 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.


pub  rsa4096/0x699D9E7F63B40E79
     created: 2021-01-21  expires: never       usage: C
     trust: unknown       validity: unknown
sub  rsa4096/0xDC52E4C251E216DF
     created: 2021-01-21  expires: never       usage: S
[ unknown] (1). FSFE OpenPGP CA <openpgp-ca@fsfe.org>

gpg> tlsign

pub  rsa4096/0x699D9E7F63B40E79
     created: 2021-01-21  expires: never       usage: C
     trust: unknown       validity: unknown
 Primary key fingerprint: A30A C9F8 FA7A 883C 0060  3070 699D 9E7F 63B4 0E79

     FSFE OpenPGP CA <openpgp-ca@fsfe.org>

Please decide how far you trust this user to correctly verify other users' keys
(by looking at passports, checking fingerprints from different sources, etc.)

  1 = I trust marginally
  2 = I trust fully

Your selection? 2

Please enter the depth of this trust signature.
A depth greater than 1 allows the key you are signing to make
trust signatures on your behalf.

Your selection? 1

Please enter a domain to restrict this signature, or enter for none.

Your selection? fsfe.org

Are you sure that you want to sign this key with your
key "Alice <alice@example.org>" (0x123456789ABCDEF0)

The signature will be marked as non-exportable.

Really sign? (y/N) y

gpg> save
```

Note that at the end, GnuPG asks you to make sure that you're making the 
trust signature with the right key (if you have multiple keys, you might 
want to specify which one to use, with the `--local-user` parameter).

Also note that you have limited the delegation of trust to the domain 
`fsfe.org`. This means that your GnuPG environment will only accept 
certifications on User IDs of the form `foo@fsfe.org` from the FSFE CA.

# Using the FSFE CA for authentication

Now that you have set the FSFE CA as a trusted introducer (scoped to
`fsfe.org`), you could first check that GnuPG considers the FSFE CA 
key `full`y trusted:

```
$ gpg -k openpgp-ca@fsfe.org
pub   rsa4096/0x699D9E7F63B40E79 2021-01-21 [C]
      Key fingerprint = A30A C9F8 FA7A 883C 0060  3070 699D 9E7F 63B4 0E79
uid                   [  full  ] FSFE OpenPGP CA <openpgp-ca@fsfe.org>
sub   rsa4096/0xDC52E4C251E216DF 2021-01-21 [S]
```

Given that, if you now import other keys from the FSFE WKD, you'll 
see that they automatically show up as authenticated in your environment 
(note that the User ID `mk@fsfe.org` immediately shows `full` trust):

```
$ gpg --locate-external-keys --auto-key-locate wkd mk@fsfe.org

gpg: key A0616A85CE41AD88: public key "Matthias Kirschner <mk@fsfe.org>" imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg: marginals needed: 3  completes needed: 1  trust model: classic
gpg: depth: 0  valid:   6  signed:   8  trust: 0-, 0q, 0n, 0m, 0f, 6u
gpg: depth: 1  valid:   8  signed:   0  trust: 8-, 0q, 0n, 0m, 0f, 0u
gpg: next trustdb check due at 2021-04-11
pub   rsa4096 2014-01-04 [SC] [expires: 2023-01-05]
      C922F488F932F95D6F7C10F7A0616A85CE41AD88
uid           [ full] Matthias Kirschner <mk@fsfe.org>
sub   rsa4096 2014-01-04 [E] [expires: 2023-01-05]```
```

# Some more thoughts on security tradeoffs

Of course, different people have different security needs. This tutorial 
is aiming mostly at making authentiated use of OpenPGP easier. There are 
many trade-offs that one can think about. For example, you could of course 
set the FSFE CA key only to a "marginal" level of trust, if your setup 
uses marginal trust.

The FSFE CA is set up to work as an automated system. It is not an 
airgapped system. Its certifications do not aim to confer an extreme 
level of confidence.
Rather, its certifications signify approximately the level of security 
that the FSFE's member portal has (the FSFE's member portal can be used 
to upload member's OpenPGP keys, which then get automatically certified by 
the FSFE OpenPGP CA instance).

Note that keys certified by the FSFE CA have much stronger security 
properties than, for example, a key that was obtained by just querying a 
non-validating keyserver (such as the SKS pool) for a key with a given 
email address.

A certification by the FSFE CA demonstrates one of two things:

1) A key has been signed by the FSFE CA because someone had access to one 
   particular login at the FSFE portal, or
2) Someone has obtained access to the private key material of the FSFE CA 
   and used it to make that certification.

# Potential pitfall: GnuPG "trust model"

Especially if your GnuPG environment is old, it's possible that support for 
trust signatures is accidentally disabled.
To check for this, call `gpg --check-trustdb`. If that shows
`trust model: classic`, then trusted introducers are disabled in your setup:

```
$ gpg --check-trustdb
gpg: marginals needed: 3  completes needed: 1  trust model: classic
gpg: depth: 0  valid:   6  signed:   9  trust: 0-, 0q, 0n, 0m, 0f, 6u
gpg: depth: 1  valid:   9  signed:   2  trust: 9-, 0q, 0n, 0m, 0f, 0u
gpg: next trustdb check due at 2021-04-11
```

If you want to use trusted introducers (which is needed to benefit from the 
certifications that OpenPGP CA uses), you need to adjust the GnuPG 
"trust model" setting.

The "modern" default GnuPG Trust model, that you probably want to switch 
to is `pgp` (or, alternatively you could use `pgp+tofu`).

There are two mechanisms by which the trust model could be configured in 
GnuPG:

1) The `~/.gnupg/gpg.conf` configuration file, which you can edit directly.

2) A setting within `trustdb.gpg`, which you can change by calling 
   e.g. `gpg --check-trustdb --trust-model=pgp`.
